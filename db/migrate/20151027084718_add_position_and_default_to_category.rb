class AddPositionAndDefaultToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :position, :integer, default: nil
    add_column :categories, :default, :boolean, default: false
  end
end
