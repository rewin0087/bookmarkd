class AddIconPublicPrivateBroadcastToBookmark < ActiveRecord::Migration
  def change
    add_column :bookmarks, :icon, :string
    add_column :bookmarks, :public, :boolean
    add_column :bookmarks, :private, :boolean
    add_column :bookmarks, :broadcast, :boolean
  end
end
