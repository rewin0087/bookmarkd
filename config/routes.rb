Rails.application.routes.draw do
  devise_for :user, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    password: 'password',
    confirmation: 'verification'
  }

  resources :profile, only: [:show, :edit, :update] do
    member do
      put 'update_password'
    end
  end

  resources :categories, except: [:index] do
    resources :bookmarks

    post 'sub', on: :collection
  end

  root 'home#index'
end
