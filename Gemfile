source 'https://rubygems.org'

gem 'ejs'
gem 'fastimage' # image manipulation through uri
gem 'htmlcompressor'
gem 'yui-compressor' # compressor
gem 'nokogiri' # html parser
gem 'rails', '4.2.2'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'therubyracer', platforms: :ruby
gem 'jquery-turbolinks'
gem 'jquery-rails'
gem 'turbolinks'
gem 'thin', '~> 1.6.2'
gem 'figaro' # environment config
gem "select2-rails" # dropdown
gem 'devise' # authentication
gem 'rails-timeago', '~> 2.0' # lazy updating of time e.g. 4 minutes ago
gem 'kaminari' # pagination
gem 'simple_form' # form helper
gem 'bootstrap-sass', '~> 3.3' # bootstrap
gem 'ionicons-rails' # icons
gem 'slim-rails' # template engine
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'rb-fsevent'
gem 'friendly_id', '~> 5.1.0' # pretty url
gem 'jscrollpane-rails' # scrollpane js
gem 'jquery-mousewheel-rails' # mousewheel for scrollpane js
gem 'gon'
gem 'cancancan', '~> 1.10' # authorization
gem 'carrierwave' # uploader
gem "mini_magick" # image manipulation for carrierwave
gem 'open_uri_redirections' # nokigir open-uri redirection

group :production do
  ruby '2.2.1'
  # static assests
  gem 'rails_12factor'
  # Use postgresql as the database for Active Record
  gem 'pg'
  gem 'heroku_secrets', github: 'alexpeattie/heroku_secrets'
end

group :development do
  gem 'mysql2', '~> 0.3.13' # database
  gem 'rack-mini-profiler'
  gem 'query_diet'
  gem 'terminal-notifier-guard'
  gem 'spring'
  gem 'letter_opener'
end

group :development, :test do
  gem 'quiet_assets'
  gem 'byebug'
  gem 'factory_girl_rails'
  gem 'ffaker'
  gem 'rspec-rails', '~> 3.1.0'
  gem 'rspec', '~> 3.1.0'
  gem 'pry-rails'
  gem 'pry-remote'
  gem 'brakeman'
  gem 'guard'
  gem 'guard-rspec'
  gem 'rspec-activemodel-mocks'
end

group :test do
  gem 'shoulda'
  gem 'sqlite3'
  gem 'simplecov', require: false
  gem 'simplecov-rcov', require: false
  gem 'ci_reporter_rspec'
  gem 'rspec-its'
  gem 'webmock'
  gem 'timecop'
  gem 'capybara'
  gem 'capybara-email'
  gem 'database_cleaner'
end

