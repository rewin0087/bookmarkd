window.setActiveAnchorTag = () ->
  currentPath = window.location.pathname
  $('a').each (e) ->
    if $(@).attr('href') == currentPath
      $(@).addClass('active')

$(document).on 'page:load ready', () ->
  setActiveAnchorTag()