window.sidebarScroll = () ->
  sidebar = $('.sidebar')
  mainCategory = sidebar.find('#main-categories')
  sidebarHeight = sidebar.innerHeight() - offsetWidth()
  mainCategory.css({height: "#{sidebarHeight}px"})

  if !!mainCategory.data('jsp')
    jsp = mainCategory.data('jsp')
    jsp.destroy()
    $('#main-categories').css({height: "#{sidebarHeight}px"}).jScrollPane()

  mainCategory.jScrollPane()

window.offsetWidth = () ->
    width = $(document).innerWidth()
    if width <= 1024 && width > 570
      offset = 320
    else if width <= 570 && width > 490
      offset = 320
    else if width <= 490
      offset = 320
    else
      offset = 150
    offset

window.sidebar = () ->
  window.onresize = (e) ->
    sidebarScroll()
  sidebarScroll()

window.pageLimitDropdown = () ->
  select = $('select#page_limit')
  select.on 'change', () ->
    val = $(@).find('option:selected').attr('href')
    Turbolinks.visit(val)

window.setActiveCategories = () ->
  if !!gon.params
    defaultCategory = gon.params.default
    mainCategory = gon.params.category
    subCategory = gon.params.sub_category

    if !!defaultCategory
      $("a[data-default-category='#{defaultCategory}']").addClass('active')

    if !!mainCategory
      $("a[data-category='#{mainCategory}']").addClass('active')

    if !!subCategory
      $("a[data-sub-category='#{subCategory}']").addClass('active')

$(document).on 'page:load ready', () ->
  sidebar()
  setActiveCategories()
  pageLimitDropdown()

  $('.sidebar-toggle a').on 'click', (e) ->
    sidebar = $('.sidebar')
    toggle = $('.sidebar-toggle')

    if sidebar.hasClass('on')
      sidebar.removeClass('on')
      toggle.removeClass('on')
    else
      sidebar.addClass('on')
      toggle.addClass('on')

$(document).on 'scroll', (e) ->
  if $('body').hasClass('categories')
    if($(document).scrollTop() > 70)
      $('div.new-bookmark-form').addClass('new-bookmark-form-fixed')
    else
      $('div.new-bookmark-form').removeClass('new-bookmark-form-fixed')

