# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'page:load ready', () ->
  $('#bookmark_category_id').select2()
  $('.bookmark-form form').on 'ajax:error', () ->
    _m.render_error 'An Error occured while processing your request, please try again later.'

  $("input#bookmark_url").focus () ->
    $(@).select()