class ProfileController < ApplicationController
  before_filter :set_user

  def show
    authorize! :read, @user
  end

  def edit
    authorize! :manage, @user
  end

  def update
    authorize! :manage, @user
    @user.assign_attributes(basic_information_params)
    @user.valid?
    @user.save
    if @user.errors.empty?
      flash.now[:success] = 'Successfully Updated Your Basic Information'
    end

    render :edit
  end

  def update_password
    authorize! :manage, @user

    if @user.valid_password? security_params[:current_password]
      if security_params[:password].include?(security_params[:current_password])
        @user.errors.add(:password, 'New password should not be the same with your old password.')
      else
        @user.reset_password!(security_params[:password], security_params[:password_confirmation])
        sign_in(@user, bypass: true) if @user.errors.empty?
      end
    else
      @user.errors.add(:current_password, 'Invalid current password, Please type your correct password.')
    end
  end

  private

  def set_user
    @user = User.find params[:id]
  end

  def basic_information_params
    params.require(:user).permit(:first_name, :last_name, :primary_photo)
  end

  def security_params
    params.require(:user).permit(:current_password, :password, :password_confirmation)
  end
end
