class HomeController < ApplicationController
  def index
    @bookmarks = Bookmark.broadcasts.page(params[:page])
  end
end
