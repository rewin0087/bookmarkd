class BookmarksController < ApplicationController
  before_filter :set_bookmark, only: [:show, :update, :edit, :destroy]

  def index
  end

  def new
  end

  def create
    @bookmark = Bookmark.new(bookmark_params)
    @category = Category.find(params[:category_id])
    @bookmark.category = @category
    @bookmark.user = current_user
    authorize! :manage, @bookmark

    if @bookmark.valid?
      @bookmark.parse_url!
      @bookmark.save
    end
  end

  def show
    authorize! :read, @bookmark
  end

  def edit
    authorize! :manage, @bookmark
    @categories = Category.where(user: current_user)
  end

  def update
    authorize! :manage, @bookmark
    @bookmark.assign_attributes(bookmark_params)
    @bookmark.save
  end

  def destroy
    authorize! :manage, @bookmark
    @bookmark.destroy
  end

  private

  def set_bookmark
    @bookmark = Bookmark.find params[:id]
  end

  def bookmark_params
    params.require(:bookmark).permit(:url, :private, :public, :broadcast, :title, :description, :site_name, :category_id)
  end
end
