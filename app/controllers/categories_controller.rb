class CategoriesController < ApplicationController
  before_filter :set_category, only: [:edit, :update, :destroy]
  helper_method :query_category_params

  def new
  end

  def create
    @category = Category.new(category_params)
    @category.user = current_user

    authorize! :manage, @category
    if @category.valid?
      @category.save
      @default_category = @category.parent
    end
  end

  def sub
    @child_category = Category.new(category_params)
    @child_category.user = current_user

    authorize! :manage, @child_category
    if @child_category.valid?
      @child_category.save
      @parent_category = @child_category.parent
    end
  end

  def show
    @default_category = Category.find params[:id]
    @categories = @default_category.childrens(current_user)
    @parent_category = @default_category.find_by_name_and_user(params[:category], current_user) if params[:category] && @default_category
    @sub_category = @parent_category.find_by_name_and_user(params[:sub_category], current_user) if params[:sub_category] && @parent_category
    @sub_categories = @parent_category.childrens(current_user) if @parent_category

    authorize! :read, @sub_category || @parent_category if @sub_category || @parent_category
    @category = Category.new
    @child_category = Category.new
    @bookmark = Bookmark.new
    @bookmarks = []

    if @sub_category
      @bookmarks = Bookmark.for_category(@sub_category, params[:page], current_user).per(params[:limit])
    elsif @parent_category
      @bookmarks = Bookmark.for_category(@parent_category, params[:page], current_user).per(params[:limit])
    end

    gon.params = query_category_params
  end

  def edit
    authorize! :manage, @category
  end

  def update
    authorize! :manage, @category
    @category.assign_attributes(category_params)
    @category.save
  end

  def destroy
    @category.destroy
  end

  private

  def query_category_params
    { default: params[:id], category: params[:category], sub_category: params[:sub_category] }
  end

  def set_category
    @category = Category.find params[:id]
  end

  def category_params
    params.require(:category).permit(:name, :description, :parent_id)
  end
end
