class Category < ActiveRecord::Base
  extend FriendlyId

  MAIN = ['Shoe Box', 'Guilty Pleasures', 'Desert Island', 'Shop', 'Top 10', 'Favorites', 'Bucket List', 'Projects']
  PRECREATED = ['Movies', 'Music', 'Books', 'Videos', 'Websites']

  belongs_to :user
  belongs_to :parent, class_name: 'Category', foreign_key: 'parent_id'
  has_many :bookmarks, dependent: :destroy
  has_many :childrens, class_name: 'Category', foreign_key: 'parent_id', dependent: :destroy

  validates :name, presence: true
  validates :name, uniqueness: { scope: [:parent_id, :user_id] }
  friendly_id :slug_candidates, use: [:history]

  MAIN.each do |category|
    define_singleton_method "#{category.gsub(' ', '_').underscore}" do
      find_by(name: category, default: true, parent: 0, user_id: nil)
    end

    define_singleton_method "#{category.gsub(' ', '_').underscore}?" do
      category.gsub(' ', '_').underscore
    end
  end

  def slug_candidates
    [
      :name,
      [:name, :id],
      [:name, :id, :user_id]
    ]
  end

  def self.main_defaults
    where(parent_id: 0, default: true, user_id: nil)
  end

  def self.generate_precreated(user)
    main_defaults.each do |main_category|
      precreated = PRECREATED.map {|p| { name: p, parent: main_category, user: user } }
      create(precreated)
    end
  end

  def childrens(user = nil)
    if user
      Category.where(parent: self, user: user)
    else
      super
    end
  end

  def find_by_name_and_user(category_name, user)
    Category.find_by(name: category_name, user: user, parent: self)
  end

  def self.find_for_user(category_id, user)
    find_by(id: category_id, user: user)
  end
end
