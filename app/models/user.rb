class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :first_name
  validates_presence_of :last_name

  has_many :categories
  has_many :bookmarks

  after_create :set_default_categories
  mount_uploader :primary_photo, ::UserAvatarUploader

  def set_default_categories
    Category.generate_precreated(self)
  end

  def full_name
    "#{first_name.humanize} #{last_name.humanize}"
  end
end
