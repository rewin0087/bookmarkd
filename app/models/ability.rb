class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, Bookmark do |bookmark|
      if bookmark.broadcast || bookmark.public
        true
      elsif bookmark.user == user
        true
      end
    end

    can :manage, Bookmark do |bookmark|
      if bookmark.category.user == user && bookmark.user == user
        true
      end
    end

    can :manage, Category do |category|
      if category.parent.default == false && category.user == user
        true
      elsif category.parent.default == true && category.default == false && category.user == user
        true
      end
    end

    can :read, User do |profile|
      if profile == user
        true
      end
    end

    can :manage, User do |profile|
      if profile == user
        true
      end
    end
  end
end
