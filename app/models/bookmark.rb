require 'open-uri'

class Bookmark < ActiveRecord::Base
  default_scope { order('created_at desc') }
  scope :broadcasts, -> { where(broadcast: true).order('created_at desc') }
  scope :privates, -> { where(private: true).order('created_at desc') }
  scope :publics, -> { where(public: true).order('created_at desc') }

  belongs_to :category
  belongs_to :user

  validates :url, presence: true
  validates :url, format: { with: URI.regexp }, if: Proc.new { |a| a.url.present? }

  serialize :preview, Array

  paginates_per 16

  def self.for_category(category, page = 0, user = nil)
    where(category: category, user: user).page(page)
  end

  def url_name
    url.gsub('http://', '') || url.gsub('https://', '')
  end

  def title
    super.blank? ? site_name : super
  end

  def parse_url!
    thumbnailer = LinkScraper::Thumbnailer.parse_url!(url)
    self.title = thumbnailer.title
    self.description = thumbnailer.description
    self.site_name = thumbnailer.site_name
    self.video = thumbnailer.video
    self.page_type = thumbnailer.page_type
    self.icon = thumbnailer.icon
    self.preview = thumbnailer.images
    self
  end
end
