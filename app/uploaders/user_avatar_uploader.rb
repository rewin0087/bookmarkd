# encoding: utf-8

class UserAvatarUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog
  process resize_to_fit_with_contrast: [600, 600]

  version :medium do
    process resize_to_fit: [250, 250]
  end

  version :small do
    process resize_to_fit: [160, 160]
  end

  version :thumb do
    process resize_to_fit: [70, 70]
  end

  version :medium_grid do
    process :resize_to_fill => [250, 250]
    process :resize_and_crop => 250
  end

  version :small_grid do
    process resize_to_fit: [160, 160]
    process :resize_and_crop => 160
  end

  version :thumb_grid do
    process :resize_to_fill => [70, 70]
    process :resize_and_crop => 70
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
     "#{secure_token}#{File.extname(original_filename)}" if original_filename.present?
  end

  protected

  def secure_token(length=16)
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  private

  def resize_and_crop(size)
    manipulate! do |image|
      if image[:width] < image[:height]
        remove = ((image[:height] - image[:width])/2).round
        image.shave("0x#{remove}")
      elsif image[:width] > image[:height]
        remove = ((image[:width] - image[:height])/2).round
        image.shave("#{remove}x0")
      end
      image.resize("#{size}x#{size}")
      image
    end
  end

  def resize_to_fit_with_contrast(width, height)
    manipulate! do |img|
      img.combine_options do |c|
        c.quality '90%'
        c.depth '8'
        c.interlace 'plane'
        c.contrast
        c.resize    "#{width}x#{height}>"
        c.resize    "#{width}x#{height}<"
      end

      img
    end
  end

end
