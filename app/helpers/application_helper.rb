module ApplicationHelper
  def site_title(page_title = '', separator = " | ")
    base_title = ENV['SITE_TITLE'] || 'Bookmarkd'
    if page_title.empty?
      base_title
    else
      [page_title, base_title].join(separator)
    end
  end
end
