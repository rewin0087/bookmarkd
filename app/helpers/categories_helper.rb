module CategoriesHelper
  def main_nav_category_link_to(category)
    link_to category.name.upcase, category_path(category, default: 1, category: params[:category], sub_category: params[:sub_category]), class: 'main-category', data: { default_category: category.to_param }, title: category.name
  end

  def category_link_to(category)
    link_to category.name.humanize, category_path(category.parent, category: params[:category] == category.name ? nil : category.name, sub_category: params[:sub_category]), class: 'btn btn-sm btn-default', data: { category: category.name }
  end

  def sub_category_link_to(category)
    if category.parent.parent
      link_to category.name.humanize, category_path(category.parent.parent, category: category.parent.name, sub_category: params[:sub_category] == category.name ? nil : category.name), class: 'btn btn-sm btn-default', data: { sub_category: category.name }
    end
  end
end
